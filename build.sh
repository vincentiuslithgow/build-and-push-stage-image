#!/bin/sh

if [[ -z "${BUILD_TAG}" ]]; then
  BUILD_TAG="${CI_COMMIT_SHORT_SHA}"
fi

echo -e "\nLog into docker registry . . ."
cat secret.txt | docker login --username vincentiusindra --password-stdin

echo -e "\nBuild image ${APP_NAME}:${CI_COMMIT_SHORT_SHA}"
docker build -t vincentiusindra/${APP_NAME}:${BUILD_TAG} .

echo -e "\nPush image ${APP_NAME}:${CI_COMMIT_SHORT_SHA}"
docker push vincentiusindra/${APP_NAME}:${BUILD_TAG}

echo -e "\nTag image as latest image"
docker tag vincentiusindra/${APP_NAME}:${BUILD_TAG} vincentiusindra/${APP_NAME}:latest

echo -e "\nPush image ${APP_NAME}:latest"
docker push vincentiusindra/${APP_NAME}:latest
